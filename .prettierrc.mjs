export default {
  tabWidth: 2,
  useTabs: false,
  tailwindConfig: './packages/@alexlab-cloud/jsonresume-astrowind/tailwind.config.ts',
  trailingComma: 'es5',
  semi: true,
  singleQuote: true,
  quoteProps: 'consistent',
  bracketSpacing: true,
  arrowParens: 'always',
  printWidth: 120,
  plugins: [
    'prettier-plugin-astro',
    'prettier-plugin-tailwindcss', // ⚠️ Order is very important: place Tailwind prettier plugin at the end
  ],
  overrides: [
    {
      files: '*.astro',
      options: {
        parser: 'astro',
      },
    },
  ],
};
