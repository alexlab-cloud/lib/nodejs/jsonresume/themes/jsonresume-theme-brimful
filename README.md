# career.jsonresume-kit

> Packages for publishing your own [JSON Resume][links.jsonresume]

A monorepository with packages designed to produce clean and accessible JSON Resume webpages.

This project builds on JSON Resume's aim to separate resume information from formatting, offering additional
automation and tools you can use to get started quickly.

[links.jsonresume]: https://jsonresume.org/
