import type { Resume } from '@alexlab-cloud/jsonresume-utils';

export type PageMetadata = {};

export type Env = {
  metadata: PageMetadata;
  jsonresume: Resume;
};
