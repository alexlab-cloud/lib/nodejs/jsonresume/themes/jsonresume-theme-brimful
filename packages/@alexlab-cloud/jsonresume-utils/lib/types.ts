/* Type definition module for data that adheres to the JSONResume schema → https://jsonresume.org/schema/ */

export type Location = {
  address?: string;
  postalCode?: string;
  city?: string;
  countryCode?: string;
  region?: string;
};

export type Profile = {
  network: string;
  username: string;
  url?: string;
};

export type Basics = {
  name: string;
  label?: string;
  image?: string;
  email?: string;
  phone?: string;
  url?: string;
  summary?: string;
  location?: Location;
  profiles?: Profile[];
};

export type Skill = {
  name: string;
  level?: string;
  keywords?: string[];
};

export type Work = {
  name: string;
  position: string;
  url?: string;
  startDate: Date;
  endDate: Date;
  summary: string;
  highlights: string[];
};

export type Volunteering = {
  organization: string;
  position: string;
  url?: string;
  startDate: Date;
  endDate: Date;
  summary: string;
  highlights?: string;
};

export type Project = {
  name: string;
  startDate?: Date;
  endDate?: Date;
  description?: string;
  highlights?: string[];
  url?: string;
};

export type Education = {
  institution?: string;
  url?: string;
  area?: string;
  studyType?: string;
  startDate?: Date;
  endDate?: Date;
  score?: string;
  courses?: string;
};

export type Award = {
  title: string;
  date?: Date;
  awarder?: string;
  summary?: string;
};

export type Certificate = {
  name: string;
  date: Date;
  issuer: string;
  url: URL;
};

export type Publication = {
  name: string;
  publisher: string;
  releaseDate?: Date;
  url?: string;
  summary?: string;
};

export type Language = {
  language: string;
  fluency: string;
};

export type Interest = {
  name: string;
  keywords: string[];
};

export type Reference = {
  name: string;
  reference: string;
};

export type Resume = {
  basics: Basics;
  skills?: Skill[];
  work?: Work[];
  volunteer?: Volunteering[];
  projects?: Project[];
  education: Education[];
  awards?: Award[];
  certificates?: Certificate[];
  publications?: Publication[];
  languages?: Language[];
  interests?: Interest[];
  references?: Reference[];
};
